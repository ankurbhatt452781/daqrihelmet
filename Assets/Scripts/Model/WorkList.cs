﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace FPL.DAQRI
{

	public class WorkListLocation
	{

		[JsonProperty("feeder_number")]
		public string feeder_number { get; set; }

		[JsonProperty("address")]
		public string address { get; set; }

		[JsonProperty("wk_loc_id")]
		public string wk_loc_id { get; set; }

		[JsonProperty("dvc_name")]
		public string dvc_name { get; set; }

		[JsonProperty("fpl_id")]
		public string fpl_id { get; set; }

		[JsonProperty("status")]
		public string status { get; set; }

		[JsonProperty("latlng_x")]
		public string latlng_x { get; set; }

		[JsonProperty("latlng_y")]
		public string latlng_y { get; set; }

		[JsonProperty("ddb_coordinate")]
		public string ddb_coordinate { get; set; }
	}

	public class Locations
	{

		[JsonProperty("WorkListLocation")]
		public object WorkListLocation { get; set; }
	}

	public class WorkListItem
	{

		[JsonProperty("wk_queue_id")]
		public string wk_queue_id { get; set; }

		[JsonProperty("wk_num")]
		public string wk_num { get; set; }

		[JsonProperty("status")]
		public string status { get; set; }

		[JsonProperty("wk_seq_id")]
		public string wk_seq_id { get; set; }

		[JsonProperty("wk_seq_name")]
		public string wk_seq_name { get; set; }

		[JsonProperty("substation")]
		public string substation { get; set; }

		[JsonProperty("Locations")]
		public Locations Locations { get; set; }
	}

	public class WorkList
	{

		[JsonProperty("WorkList")]
		public IList<WorkListItem> WorkListItem { get; set; }
	}

}