﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScatterSequencer : MonoBehaviour {

	public GameObject ScatterMenu;

	public GameObject TargetMap;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void changePositionOnBtnClick () {
		LeanTween.moveX (ScatterMenu, -1.0f, 1f).setEaseOutQuad();
		LeanTween.moveX (TargetMap, 0.4f, 1f).setEaseOutQuad ();
	}

	public void RestorePosition () {
		LeanTween.moveX (ScatterMenu, 0.0f, 1f).setEaseOutQuad();
	}
}
