﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inspection : MonoBehaviour {

	public GameObject ThermalGameObj;
	public GameObject DepthGameObj;
	public GameObject Vegetation;
	public GameObject LightningArrester;
	public GameObject Pole;
	public GameObject Insulator;
	public GameObject Capacitor;
	public GameObject CrossArm;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void InspectionBtnClick (int gameObjtag) {

		switch (gameObjtag) {
		case 1:
			ThermalGameObj.SetActive ((ThermalGameObj.activeSelf == true) ? false : true);
			DepthGameObj.SetActive (false);
			Vegetation.SetActive (false);
			LightningArrester.SetActive (false);
			Pole.SetActive (false);
			Insulator.SetActive (false);
			Capacitor.SetActive (false);
			CrossArm.SetActive (false);
			break;
		case 2:
			DepthGameObj.SetActive ((DepthGameObj.activeSelf == true) ? false : true);
			ThermalGameObj.SetActive (false);
			Vegetation.SetActive (false);
			LightningArrester.SetActive (false);
			Pole.SetActive (false);
			Insulator.SetActive (false);
			Capacitor.SetActive (false);
			CrossArm.SetActive (false);
			break;
		case 3:
			Vegetation.SetActive ((Vegetation.activeSelf == true) ? false : true);
			ThermalGameObj.SetActive (false);
			DepthGameObj.SetActive (false);
			LightningArrester.SetActive (false);
			Pole.SetActive (false);
			Insulator.SetActive (false);
			Capacitor.SetActive (false);
			CrossArm.SetActive (false);
			break;
		case 4:
			LightningArrester.SetActive ((LightningArrester.activeSelf == true) ? false : true);
			ThermalGameObj.SetActive (false);
			DepthGameObj.SetActive (false);
			Vegetation.SetActive (false);
			Pole.SetActive (false);
			Insulator.SetActive (false);
			Capacitor.SetActive (false);
			CrossArm.SetActive (false);
			break;
		case 5:
			Pole.SetActive ((Pole.activeSelf == true) ? false : true);
			ThermalGameObj.SetActive (false);
			DepthGameObj.SetActive (false);
			Vegetation.SetActive (false);
			LightningArrester.SetActive (false);
			Insulator.SetActive (false);
			Capacitor.SetActive (false);
			CrossArm.SetActive (false);
			break;
		case 6:
			Insulator.SetActive ((Insulator.activeSelf == true) ? false : true);
			ThermalGameObj.SetActive (false);
			DepthGameObj.SetActive (false);
			Vegetation.SetActive (false);
			LightningArrester.SetActive (false);
			Pole.SetActive (false);
			Capacitor.SetActive (false);
			CrossArm.SetActive (false);
			break;
		case 7:
			Capacitor.SetActive ((Capacitor.activeSelf == true) ? false : true);
			ThermalGameObj.SetActive (false);
			DepthGameObj.SetActive (false);
			Vegetation.SetActive (false);
			LightningArrester.SetActive (false);
			Pole.SetActive (false);
			Insulator.SetActive (false);
			CrossArm.SetActive (false);
			break;
		case 8:
			CrossArm.SetActive ((CrossArm.activeSelf == true) ? false : true);
			ThermalGameObj.SetActive (false);
			DepthGameObj.SetActive (false);
			Vegetation.SetActive (false);
			LightningArrester.SetActive (false);
			Pole.SetActive (false);
			Insulator.SetActive (false);
			Capacitor.SetActive (false);
			break;
		default:
			break;
		}
	}

	private void allGameObjToInactive () {
		
		ThermalGameObj.SetActive (false);
		DepthGameObj.SetActive (false);
		Vegetation.SetActive (false);
		LightningArrester.SetActive (false);
		Pole.SetActive (false);
		Insulator.SetActive (false);
		Capacitor.SetActive (false);
		CrossArm.SetActive (false);
	}
}
