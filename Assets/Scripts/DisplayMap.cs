﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DAQRI;
using FPL.DAQRI;
using System;
using UnityEngine.UI;
using System.Text;

public class DisplayMap : MonoBehaviour
{

	public WorkList WorkList;
	ArrayList coordinatesArr = new ArrayList ();

	void Start ()
	{

		WSCall wsCall = FindObjectOfType<WSCall> ();
		WorkList = wsCall.WorkList;

		img = gameObject.GetComponent<RawImage> ();
		StartCoroutine (GetCountries ());
	}

	void Awake ()
	{
		DontDestroyOnLoad (this);
	}

	IEnumerator GetCountries ()
	{
		for (int i = 0; i < WorkList.WorkListItem.Count; i++) {

			WorkListItem WorkItem = WorkList.WorkListItem [i];
			var token = JToken.Parse (WorkItem.Locations.WorkListLocation.ToString ());
			Debug.Log ("token is" + token);

			if (token is JArray) {

				var tokenArr = token.Value<JArray> ();
				for (int j = 0; j < tokenArr.Count; j++) {

					WorkListLocation workLoc = tokenArr [j].ToObject<WorkListLocation> ();
					coordinatesArr.Add (workLoc);
				}
			} else if (token is JObject) {
				WorkListLocation workLocation = token.ToObject<WorkListLocation> ();
				coordinatesArr.Add (workLocation);
			}
		}
		double[] FeederLat = new double[coordinatesArr.Count];
		double[] FeederLon = new double[coordinatesArr.Count];

		for (int i = 0; i < coordinatesArr.Count; i++) {
			WorkListLocation workLocation = (WorkListLocation)coordinatesArr [i];
			Debug.Log (workLocation.latlng_x);
			Debug.Log (workLocation.latlng_y);
			FeederLat [i] = Convert.ToDouble (workLocation.latlng_y);
			FeederLon [i] = Convert.ToDouble (workLocation.latlng_x);
		}

		MapMarkers.Append ("&markers=color:red%7Clabel:S%7C");
		for (int i = 0; i < FeederLat.Length; i++) {
			String lat = FeederLat [i].ToString ();
			String lon = FeederLon [i].ToString ();
			MapMarkers = MapMarkers.Append (lat);
			MapMarkers = MapMarkers.Append (",");
			MapMarkers = MapMarkers.Append (lon);
			if (i < FeederLat.Length - 1) {
				MapMarkers = MapMarkers.Append ("%7C");
			}
		}
		MapMarkers.Remove (MapMarkers.Length - 3, 3);
		url1.Append ("https://maps.googleapis.com/maps/api/staticmap?center=");
		url1.Append (FeederLat[0]);
		url1.Append (",");
		url1.Append (FeederLon[0]);
		url1.Append ("&zoom=");
		url1.Append (zoom);
		url1.Append ("&size=");
		url1.Append (mapWidth);
		url1.Append ("x");
		url1.Append (mapHeight);
		url1.Append ("&scale=");
		url1.Append (scale);
		url1.Append ("&maptype=");
		url1.Append (mapSelected);
		url1.Append (MapMarkers);
		url1.Append ("&key=AIzaSyDD1X9FLCF7Po7ZUXapjRAUyyOt6Qi4EcA");
		url = url1.ToString ();
		Debug.Log (url);
		WWW www1 = new WWW (url);
		yield return www1;
		img.texture = www1.texture;
		img.SetNativeSize ();

	}

	//----------------------------------------------------------------------------------
	//public WorkList WorkList;
	//ArrayList coordinatesArr = new ArrayList();
	// Use this for initialization
	public RawImage img;
	//current lat long
	static double myLat = 29.1921702797642f;
	static double mylong = -80.9903897474509f;
	//12.914154, 80.220725
	//>10 - 12.913355, 80.219265
	//<10 - 12.913316, 80.219252
	//double FeederLat[3] = new double{12.913316f ,12.913316f ,12.913355f};
	//double[] FeederLat = new double[3];
	//double[] FeederLong = new double[3];
	//static double[] FeederLat= { 12.913316f ,12.913316f ,12.913355f};
	//static double[] FeederLong= { 80.219252f ,80.219252f ,80.219265f};

	public static int zoom = 8;
	public static int mapWidth = 600;
	public static int mapHeight = 650;

	public  enum mapType
	{
		roadmap,
		satellite,
		hybrid,
		terrain

	}

	public static mapType mapSelected;
	public static int scale;
	static LocationInfo li;
	static string url;
	static string url2;
	static StringBuilder MapMarkers = new StringBuilder ();
	static StringBuilder url1 = new StringBuilder ();
}
