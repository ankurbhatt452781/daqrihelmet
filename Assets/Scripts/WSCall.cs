﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json;
using DAQRI;
using FPL.DAQRI;

public class WSCall : MonoBehaviour {

	public WorkList WorkList;

	void Start () {
		Debug.Log("WS CALL Start");
		StartCoroutine(GetCountries());
	}

	void Awake () {
		DontDestroyOnLoad (this);
	}

	IEnumerator GetCountries()
	{
		string getCountriesUrl = "http://40.76.46.207:8080/DaqriFplRestService/rest/work/listresult";
		using (UnityWebRequest www = UnityWebRequest.Get(getCountriesUrl))
		{
			yield return www.Send();
			if (www.isNetworkError)
			{
				Debug.Log(www.error);
			}
			else
			{
				if (www.isDone)
				{
					string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
					Debug.Log(jsonResult);
					WorkList = JsonConvert.DeserializeObject<WorkList> (jsonResult);
				}
			}
		}
	}
}