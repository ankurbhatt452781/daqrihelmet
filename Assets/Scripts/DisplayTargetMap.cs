﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayTargetMap : MonoBehaviour {

	// Use this for initialization
	public RawImage img;

	string url;
	public float lat;
	public float lon;

	private int zoom = 16;
	private int mapWidth = 300;
	private int mapHeight = 300;

	public enum mapType {roadmap,satellite,hybrid,terrain}
	public mapType mapSelected;
	public int scale;

	IEnumerator Map()
	{
		url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon + "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale +"&maptype=" + mapSelected +
			"&markers=color:red%7Clabel:S%7C"+lat+","+lon+"&key=AIzaSyBn_IRmZXiOFn1EHIONuYEfyt9VArfpMX4";
		WWW www = new WWW (url);
		yield return www;
		img.texture = www.texture;
		print (url);
	}
	// Use this for initialization
	void Start () {
		img = gameObject.GetComponent<RawImage> ();
		StartCoroutine (Map());
	}
	//AIzaSyBn_IRmZXiOFn1EHIONuYEfyt9VArfpMX4
	// Update is called once per frame
	void Update () {

	}
}